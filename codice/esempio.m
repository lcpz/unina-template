function bipoint( point, radius, color1, color2 )

    %% BIPOINT
    % Plots an half-stable point.

    %% Plot

    hold on;

    phi = linspace(pi/2, -pi/2);

    x = radius * cos(phi) + point(1); x = [x, x(1)];
    y = radius * sin(phi) + point(2); y = [y, y(1)];
    patch(x, y, color1);

    x2 = radius * cos(pi + phi) + point(1); x2 = [x2, x2(1)];
    y2 = radius * sin(pi + phi) + point(2); y2 = [y2, y2(1)];
    patch(x2, y2, color2);

end
