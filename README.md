UniNa template
==============

Un template [LaTeX](http://www.latex-project.org) per produrre relazioni e presentazioni in stile [UniNa](https://www.unina.it).

Compressione, ottimizzazione e conversione in PDF/A-1b
-----------------------------

Richiede: [ghostscript](http://www.ghostscript.com).

```shell
gs -sDEVICE=pdfwrite -dBATCH -dNOPAUSE -dSAFER -sColorConversionStrategy=UseDeviceIndependentColor \
   -dEmbedAllFonts=true -dPrinted=true -dPDFA -sProcessColorModel=DeviceRGB -dPDFACompatibilityPolicy=1 \
   -dDetectDuplicateImages -dFastWebView=true -r150 -sOutputFile=output.pdf input.pdf
```
